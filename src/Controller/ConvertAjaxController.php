<?php

namespace Drupal\language_converter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\language_converter\ConverterClient as Client;

/**
 * Convert Ajax Controller.
 */
class ConvertAjaxController extends ControllerBase {

  /**
   * Provides an ajax to the form.
   */
  public static function ajaxCommand(array &$form, FormStateInterface $formState) {
    // Config setting for language converter.
    $config = \Drupal::config('languageconverter.settings');
    $client = new Client($config->get('google_api_key'));
    // Common fields to apply the language converter.
    $fields = language_converter__common_fields();
    $response = new AjaxResponse();
    // To get the language ID.
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if (!empty($formState->getValue('temporary_field')) && ($language != $config->get('language_choice'))) {
      $translate = $client->translate($formState->getValue('temporary_field'), $config->get('language_choice'), $source);
      foreach ($fields as $field) {
        if (!empty($form[$field]['widget'][0]['value'])) {
          $form[$field]['widget'][0]['value']['#value'] = $translate;
        }
        else {
          $form[$field]['#value'] = $translate;
        }
        // Replace the old field with new field settings.
        $response->addCommand(new ReplaceCommand('.translate-target', $form[$field]));
        break;
      }
    }
    return $response;
  }

}
