<?php

namespace Drupal\language_converter\Exception;

/**
 * Google Translate API Client.
 */
class LanguagesErrorException extends \DomainException {

  /**
   * Construct of the Exception.
   *
   *  @inheritdoc
   */
  public function __construct(
    $message = 'Languages Error',
    $code = 5,
    \Exception $previous = NULL
  ) {
    parent::__construct($message, $code, $previous);
  }

}
