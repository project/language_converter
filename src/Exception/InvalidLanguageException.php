<?php

namespace Drupal\language_converter\Exception;

/**
 * Google Translate API Client.
 */
class InvalidLanguageException extends \InvalidArgumentException {

  /**
   * Construct of the Exception.
   *
   * @inheritdoc
   */
  public function __construct(
    $message = 'Invalid language',
    $code = 3,
    \Exception $previous = NULL
  ) {
    parent::__construct($message, $code, $previous);
  }

}
