<?php

namespace Drupal\language_converter\Exception;

/**
 * Google Translate API PHP Client.
 */
class InvalidSourceLanguageException extends InvalidLanguageException {

  /**
   * Construct of the Exception.
   *
   * @inheritdoc
   */
  public function __construct(
    $message = 'Invalid source language',
    $code = 3,
    \Exception $previous = NULL
  ) {
    parent::__construct($message, $code, $previous);
  }

}
