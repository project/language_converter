<?php

namespace Drupal\language_converter\Exception;

/**
 * Google Translate API Client.
 */
class InvalidAccessKeyException extends \InvalidArgumentException {

  /**
   * Construct of the Exception.
   *
   * @inheritdoc
   */
  public function __construct(
    $message = 'Invalid access key',
    $code = 1,
    \Exception $previous = NULL
  ) {
    parent::__construct($message, $code, $previous);
  }

}
