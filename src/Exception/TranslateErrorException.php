<?php

namespace Drupal\language_converter\Exception;

/**
 * Google Translate API PHP Client.
 */
class TranslateErrorException extends \DomainException {

  /**
   * Construct of the Exception.
   *
   * @inheritdoc
   */
  public function __construct(
    $message = 'Translate Error',
    $code = 4,
    \Exception $previous = NULL
  ) {
    parent::__construct($message, $code, $previous);
  }

}
