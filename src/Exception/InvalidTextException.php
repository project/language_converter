<?php

namespace Drupal\language_converter\Exception;

/**
 * Google Translate API Client.
 */
class InvalidTextException extends \InvalidArgumentException {

  /**
   * Construct of the Exception.
   *
   * @inheritdoc
   */
  public function __construct(
    $message = 'Invalid text',
    $code = 2,
    \Exception $previous = NULL
  ) {
    parent::__construct($message, $code, $previous);
  }

}
