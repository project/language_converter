<?php

namespace Drupal\language_converter\Exception;

/**
 * Google Translate API PHP Client.
 */
class DetectErrorException extends \DomainException {

  /**
   * Construct of the Exception.
   *
   * @inheritdoc
   */
  public function __construct(
    $message = 'Detect Error',
    $code = 6,
    \Exception $previous = NULL
  ) {
    parent::__construct($message, $code, $previous);
  }

}
