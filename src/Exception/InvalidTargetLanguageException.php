<?php

namespace Drupal\language_converter\Exception;

/**
 * Google Translate API PHP Client.
 */
class InvalidTargetLanguageException extends InvalidLanguageException {

  /**
   * Construct of the Exception.
   *
   * @inheritdoc
   */
  public function __construct(
    $message = 'Invalid target language',
    $code = 3,
    \Exception $previous = NULL
  ) {
    parent::__construct($message, $code, $previous);
  }

}
