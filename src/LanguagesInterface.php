<?php

namespace Drupal\language_converter;

/**
 * Google Translate API PHP Client.
 */
interface LanguagesInterface {

  /**
   * List language supports.
   *
   * @param string|null $targetLanguage
   *   Target language. ie: te, en.
   *
   * @return array
   *   array structure return above
   *
   * @throws Exception\InvalidTargetLanguageException
   * @throws Exception\TranslateErrorException
   */
  public function languages(string $targetLanguage = NULL);

}
