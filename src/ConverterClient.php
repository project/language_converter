<?php

namespace Drupal\language_converter;

use Exception\DetectErrorException;
use Exception\LanguagesErrorException;
use Exception\TranslateErrorException;
use Exception\InvalidSourceLanguageException;
use Exception\InvalidTargetLanguageException;
use Exception\InvalidTextException;
use Exception\InvalidAccessKeyException;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Google Translate API Client.
 */
class ConverterClient implements TranslateInterface, LanguagesInterface, DetectInterface {
  /**
   * API URI.
   */
  const API_URI = 'https://www.googleapis.com/language/translate/v2';

  /**
   * Access key.
   *
   * @var string
   */
  private $accessKey;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Client constructor.
   *
   * @var string $accessKey
   * @var \GuzzleHttp\ClientInterface $httpClient
   */
  public function __construct(string $accessKey, ClientInterface $httpClient = NULL) {
    if (strlen($accessKey) !== 39) {
      throw new InvalidAccessKeyException();
    }
    $this->accessKey = $accessKey;
    $this->httpClient = new HttpClient();
    if ($httpClient) {
      $this->httpClient = $httpClient;
    }
  }

  /**
   * Client Interface.
   *
   * @return \GuzzleHttp\ClientInterface
   *   $httpClient
   */
  private function getHttpClient() {
    return $this->httpClient;
  }

  /**
   * Translate Functionality.
   *
   * @inheritdoc
   */
  public function translate($text, string $targetLanguage, &$sourceLanguage = NULL) {
    // Validate if required fields has being filled.
    if (!$text) {
      throw new InvalidTextException();
    }
    // Used to return the same type of variable used in the text.
    $onceResult = !is_array($text);
    // Prepare the string.
    $text = $this->prepareText($text);
    if (!$this->isValidLanguage($targetLanguage)) {
      throw new InvalidTargetLanguageException();
    }
    // Query params.
    $query = [
      'q' => $text,
      'target' => $targetLanguage,
    ];
    // Validate if is necessary to pass the source language.
    if ($sourceLanguage && !$this->isValidLanguage($sourceLanguage)) {
      throw new InvalidSourceLanguageException();
    }
    if ($sourceLanguage) {
      $query['source'] = $sourceLanguage;
    }

    // Add access key.
    $query['key'] = $this->accessKey;

    try {
      // Send request.
      $query = $this->httpBuildQuery($query);

      $response = $this->getHttpClient()->request(
          'POST',
          self::API_URI,
          ['query' => $query]
      );
    }
    catch (GuzzleException $e) {
      throw new TranslateErrorException('Translate error: ' . $e->getMessage(), 4, $e);
    }

    // Check response json.
    $result = json_decode($response->getBody(), TRUE);
    if (!is_array($result) ||
      !array_key_exists('data', $result) ||
      !array_key_exists('translations', $result['data'])
    ) {
      throw new TranslateErrorException('Invalid response');
    }

    // Prepare responses.
    $translations = [];
    $sources = [];
    foreach ($result['data']['translations'] as $translation) {
      $translations[] = html_entity_decode($translation['translatedText'], ENT_QUOTES, 'UTF-8');

      if (array_key_exists('detectedSourceLanguage', $translation)) {
        $sources[] = $translation['detectedSourceLanguage'];
      }
    }

    // Add source language by reference if it was not passed.
    if (!$sourceLanguage) {
      $sourceLanguage = $onceResult ? current($sources) : $sources;
    }
    return $onceResult ? current($translations) : $translations;
  }

  /**
   * Languages.
   *
   * @inheritdoc
   */
  public function languages(string $targetLanguage = NULL) {
    if ($targetLanguage && !$this->isValidLanguage($targetLanguage)) {
      throw new InvalidTargetLanguageException();
    }

    // Query params.
    $query = [
      'key' => $this->accessKey,
    ];

    if ($targetLanguage) {
      $query['target'] = $targetLanguage;
    }

    try {
      // Send request.
      $query = $this->httpBuildQuery($query);

      $response = $this->getHttpClient()->request(
        'GET',
        self::API_URI . '/languages',
        ['query' => $query]
      );
    }
    catch (GuzzleException $e) {
      throw new LanguagesErrorException('Languages error: ' . $e->getMessage(), 5, $e);
    }

    // Check response json.
    $result = json_decode($response->getBody(), TRUE);
    if (!is_array($result) ||
      !array_key_exists('data', $result) ||
      !array_key_exists('languages', $result['data'])
    ) {
      throw new LanguagesErrorException('Invalid response');
    }
    return $result['data']['languages'];
  }

  /**
   * Detect.
   *
   * @inheritdoc
   */
  public function detect($text) {
    // Validate if required fields has being filled.
    if (!$text) {
      throw new InvalidTextException();
    }
    // Used to return the same type of variable used in the text.
    $onceResult = !is_array($text);
    // Prepare the string.
    $text = $this->prepareText($text);
    // Query params.
    $query = [
      'q' => $text,
      'key' => $this->accessKey,
    ];

    try {
      // Send request.
      $query = $this->httpBuildQuery($query);
      $response = $this->getHttpClient()->request(
        'POST',
        self::API_URI . '/detect',
        ['query' => $query]
      );
    }
    catch (GuzzleException $e) {
      throw new DetectErrorException('Detect error: ' . $e->getMessage(), 6, $e);
    }

    // Check response json.
    $result = json_decode($response->getBody(), TRUE);
    if (!is_array($result) ||
      !array_key_exists('data', $result) ||
      !array_key_exists('detections', $result['data'])
    ) {
      throw new DetectErrorException('Invalid response');
    }

    $result = $result['data']['detections'];
    // Remove array of array in the results.
    $processedResult = [];
    foreach ($result as $item) {
      $processedResult[] = current($item);
    }
    return $onceResult ? current($processedResult) : $processedResult;
  }

  /**
   * Create a query string.
   *
   * @var array $params
   *
   * @return string
   *   Returns the query String.
   */
  private function httpBuildQuery(Array $params) {
    $query = [];
    foreach ($params as $key => $param) {
      if (!is_array($param)) {
        continue;
      }
      // When a param has many values,
      // it generate the query string separated to join late.
      foreach ($param as $subParam) {
        $query[] = http_build_query([$key => $subParam]);
      }
      unset($params[$key]);
    }
    // Join queries strings.
    $query[] = http_build_query($params);
    $query = implode('&', $query);
    return $query;
  }

  /**
   * Prepare text to be processed.
   *
   * @var string|array $text
   *
   * @return array
   *   Returns as array
   */
  private function prepareText($text) {
    // Convert no array text to array.
    if (!is_array($text)) {
      $text = [$text];
    }
    return $text;
  }

  /**
   * Is a valid language?
   *
   * @param string $language
   *   Language to be validate.
   *
   * @return bool
   *   Returns Boolean.
   */
  private function isValidLanguage($language) {
    $regexpValidLanguage = '%([a-z]{2})(-[a-z]{2})?%';
    return preg_match($regexpValidLanguage, $language) === 1;
  }

}
