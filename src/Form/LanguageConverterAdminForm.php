<?php

namespace Drupal\language_converter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * LanguageConverter Admin Form.
 */
class LanguageConverterAdminForm extends ConfigFormBase {

  /**
   * Settings Config Variable.
   *
   * @var DrupalSettings
   */
  protected $settings;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->settings = 'languageconverter.settings';
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'language_converter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      $this->settings,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config($this->settings);
    $languages = $this->languageManager->getLanguages();
    $languageList = [];
    if (!empty($languages)) {
      foreach ($languages as $list) {
        $languageList[$list->getId()] = $list->getName();
      }
    }
    $form['language_choice'] = [
      '#type' => 'radios',
      '#options' => $languageList,
      '#title' => $this->t('Languages'),
      '#desctiption' => $this->t('Choose the language to which it need to convert.'),
      '#default_value' => !empty($config->get('language_choice')) ? $config->get('language_choice') : $this->languageManager->getCurrentLanguage()->getId(),
    ];
    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    if (!empty($types)) {
      foreach ($types as $type) {
        $contentTypeList[$type->id()] = $type->label();
      }
    }
    $form['content_type_choice'] = [
      '#type' => 'checkboxes',
      '#options' => $contentTypeList,
      '#title' => $this->t('Content Types'),
      '#description' => $this->t('Choose content types in which the language converter requires.'),
      '#default_value' => $config->get('content_type_choice'),
    ];
    $form['google_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google API key'),
      '#description' => $this->t('Google API key used for translation.'),
      '#default_value' => $config->get('google_api_key'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->settings);
    $config->set('content_type_choice', $form_state->getValue('content_type_choice'));
    $config->set('language_choice', $form_state->getValue('language_choice'));
    $config->set('google_api_key', $form_state->getValue('google_api_key'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

}
