<?php

namespace Drupal\language_converter;

/**
 * Google Translate API PHP Client.
 */
interface TranslateInterface {

  /**
   * Translate a text or multiple texts.
   *
   * @param string|array $text
   *   String or multiple strings(array) to be translated.
   * @param string $targetLanguage
   *   Target language. ie: te, en.
   * @param null|string|array $sourceLanguage
   *   Source language. If not passed, google will try figure out it.
   *
   * @return string|array
   *   Return retails above.
   *
   * @throws Exception\InvalidTextException
   * @throws Exception\InvalidTargetLanguageException
   * @throws Exception\InvalidSourceLanguageException
   * @throws Exception\TranslateErrorException
   */
  public function translate($text, string $targetLanguage, &$sourceLanguage = NULL);

}
