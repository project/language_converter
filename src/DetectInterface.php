<?php

namespace Drupal\language_converter;

/**
 * Google Translate API Client.
 *
 * @author Praven Achanta
 */
interface DetectInterface {

  /**
   * Detected the language of a text.
   *
   * @param string|array $text
   *   String or multiple strings(array) to be detected.
   *
   * @return array
   *   array structure return above.
   *
   * @throws Exception\InvalidTextException
   * @throws Exception\DetectErrorException
   */
  public function detect($text);

}
